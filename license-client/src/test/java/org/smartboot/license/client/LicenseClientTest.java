/*
 * Copyright (c) 2020-2020, org.smartboot. All rights reserved.
 * project name: smart-license
 * file name: LicenseTest.java
 * Date: 2020-03-22
 * Author: sandao (zhengjunweimail@163.com)
 */

package org.smartboot.license.client;

import java.io.InputStream;
import java.util.Base64;

/**
 * @author 三刀
 * @version V1.0 , 2020/3/21
 */
public class LicenseClientTest {
    public static void main(String[] args) {
        InputStream inputStream = LicenseClientTest.class.getClassLoader().getResourceAsStream("license_revert.txt");
        License license = new License();
        LicenseEntity licenseData = license.loadLicense(inputStream, Base64.getDecoder().decode("MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCoKSiJdKndyvd3yU1Ua+YzidF6Aq6CJqME2q477Rl2uMndXt6+head15SqcoXVg877Unfka+h0hQrItF2ziSBXY6m5zNTC5DRVK5+mkZTqq2EHUMX2RiNHILn6EAlU+Y3kysWXkpC3DPznnLXNtwtgivKPenG8PB1QwfRzXOMIKQIDAQAB"));
        System.out.println(new String(licenseData.getData()));
    }
}
